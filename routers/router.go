package routers

import (
	"gitee.com/go-micro-services/nacos-go-micro-gin/controllers"
	"github.com/gin-gonic/gin"
)

func RoutersInit(r *gin.Engine) {
	rr := r.Group("/")
	{
		rr.GET("", controllers.ApiController{}.Index)
		rr.GET("/service", controllers.ApiController{}.FindService)
		rr.GET("/getConfig", controllers.ApiController{}.GetConfig)
		rr.GET("/searchConfig", controllers.ApiController{}.SearchConfig)
	}
}
