package conf

var (
	NacosIp                  = "127.0.0.1"
	NacosPort                = uint64(8848)
	NacosTimeoutMs           = uint64(5000)
	NacosContextPath         = "/nacos"
	NacosNamespaceId         = "ff2e8758-33c1-4a88-8005-142cbee91be9"
	NacosLogDir              = "nacos/log"
	NacosCacheDir            = "nacos/cache"
	NacosLogLevel            = "debug"
	NacosServiceName         = "nacos-go-micro-gin"
	NacosGroupName           = "dd"
	NacosNotLoadCacheAtStart = true
	NacosMetadata            = map[string]string{"idc": "shanghai"}
	NacosDataId              = "test.json"
	NacosGroup               = "tt"
)
